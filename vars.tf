variable "AWS_ACCESS_KEY_ID" {
        type =  string
}

variable "AWS_SECRET_ACCESS_KEY" {
        type =  string
}

variable "AWS_DEFAULT_REGION" {
        type = string
}
variable "AWS_KEY" {
        type = string
}

variable "AWS_AMI" {
        type = string
}

variable "AWS_INSTANCE_TYPE" {
        type = string
}